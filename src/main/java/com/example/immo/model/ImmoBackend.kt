package com.example.immo.model

interface ImmoBackend {

    /*
     * Interface contenant le prototype de la fonction du Backend : ImmoBackendImpl
     * Permet l'injection du Backend
     */
    fun estime(s:String,p:Float,b:Float,l:Float): Float
}