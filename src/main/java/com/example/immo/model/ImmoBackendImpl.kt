package com.example.immo.model

import javax.inject.Inject


class ImmoBackendImpl @Inject constructor() : ImmoBackend{

    /*
     * Fonction d'estimation du prix d'un logement en fonction
     * du type de logement, du nombre de pièces, de la surface terrain et de la surface du bâtiment
     * Retourne la somme des valeurs saisies par l'utilisateur
     * multipliées par leurs coefficients déterminer par l'estimateur
     * @param [s] string contenant le type de logement soit "maison" ou "appartement"
     * @param [x] float contenant le nombre de pièce saisi par l'utilisateur
     * @param [y] float contenant la surface terrain saisie par l'utilisateur
     * @param [z] float contenant la surface bâtiment saisie par l'utilisateur
     * Si les valeurs numériques saisies sont négatives : envoie d'une exception NumberFormatException
     * Si le type de logement saisi n'est ni "maison" ni "appartement" : envoie d'une exception IllegalArgumentException
     */

    override fun estime(s:String,p:Float,b:Float,l:Float): Float {
        if(p>=0 && b>=0 && l>=0) {
            var res = 0.0
            if(s=="maison" || s=="appartement") {
                if (s == "maison") {
                    res =p*0+b*580.5291211119505+l*141009.24586584867
                }
                if (s == "appartement") {
                    res =p*4428.462063724873+b*1607.785271706292+l*84954.69152477442
                }
                return res.toFloat()
            } else {
                throw IllegalArgumentException("Illegal accommodation for estimation")
            }
        }
         else {
            throw java.lang.NumberFormatException("Illegal number for estimation")
         }
    }
    /*
    Etude des données numériques : estimateur
    l'estimateur retourne des coefficients pour chaque caractéristique qui représente aussi leur poids
    par exemple 100000€  5pc  150m2  400m2
    l'implémentation du backend peut être : prix = 100000€ = x*nb de piece + y*surface batiment + z*surface terrain

     */
}