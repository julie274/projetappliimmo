package com.example.immo.data

sealed interface ImmoResult {

    /*
     * Interface scellée décrivant les 3 cas possibles pour les valeurs saisies
     * Estimated : valeurs saisies correctes, retourne le prix
     * FailedNumber : valeurs numériques saisies négatives, retourne un message d'erreur
     * FailedAccommodation : valeurs du type de logement incorrecte, retourne un message d'erreur
     */

    @JvmInline
    value class Estimated(val price: Float): ImmoResult

    @JvmInline
    value class FailedNumber(val faultyNb: String): ImmoResult

    @JvmInline
    value class FailedAccommodation(val faultyAcc: String): ImmoResult
}