package com.example.immo.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.immo.R
import com.example.immo.databinding.CaracteristiquesBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class Caracteristiques : Fragment(){

    /*
     * Initialisation du View Binding
     * permet de transformer les identifiants du layout en attibut de classe
     */

    private lateinit var binding : CaracteristiquesBinding

    /*
     * Gonflage du layout
     * Le message "messageError" indiquant qu'un champ est vide : non visible
     * L'image "warning" joint à "messageError" : non visible
     */

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = CaracteristiquesBinding.inflate(inflater)
        binding.messageError.visibility=View.GONE
        binding.warning.visibility=View.GONE
        return binding.root
    }

    /*
     * Si il y a un appuie sur le bouton estimeButton
     *   Tentative de navigation avec argument
     *      Initialisation des arguments
     *      Déclenchement de l'action avec argument
     *      Recherche du contrôleur de navigation courant et demande de navigation
     *   Si echec
     *      une valeur n'a pas était saisie
     *      envoie d'une exception et affichage d'un message d'erreur
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.estimeButton.setOnClickListener{
            try {
                val p = binding.nombrePieceChoisie.text.toString().toFloat()
                val b = binding.surfaceBatimentChoisie.text.toString().toFloat()
                val l = binding.nombreLotChoisie.text.toString().toFloat()
                val s = binding.accommodation.text.toString()
                val action = CaracteristiquesDirections.actionCaracteristiquesToEstimation(s,p,b,l)
                findNavController().navigate(action)
            } catch(e:IllegalArgumentException){
                binding.messageError.visibility=View.VISIBLE
                binding.warning.visibility=View.VISIBLE
            }
        }
    }

}