package com.example.immo.fragment

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.immo.R
import com.example.immo.data.ImmoResult
import com.example.immo.databinding.EstimationBinding
import com.example.immo.viewModel.ImmoViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class Estimation: Fragment() {

    private lateinit var binding : EstimationBinding
    val immoViewModel : ImmoViewModel by viewModels()

    /*
     * Gonflage du layout
     */

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = EstimationBinding.inflate(inflater)
        return binding.root
    }

    /*
     * Communication avec le ViewModel
     */

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /*
         * Récupération des arguments
         */
        val args : EstimationArgs by navArgs()

        immoViewModel.estime(args.s,args.p,args.b,args.l)

        /*
         * Observation de la LiveData du ViewModel
         * si la valeur est dans l'état FailedAccommodation ou FailedNumber
         *   afficher le message d'erreur adapté
         *   cacher affichage prix et autre message d'erreur possible
         * si la valeur est dans l'état Estimated
         *   valeur stockée dans l'EditText "estimation"
         *   afficher le layout de l'estimation
         *   cacher les message d'erreur
         */

        immoViewModel.resultEstimation.observe(viewLifecycleOwner){ value ->
            when(value) {
                is ImmoResult.FailedAccommodation -> {
                    binding.layoutErreurAccommodation.visibility=View.VISIBLE
                    binding.layoutEstimation.visibility=View.GONE
                    binding.layoutErreur.visibility=View.GONE


                }
                is ImmoResult.FailedNumber -> {
                    binding.layoutErreur.visibility=View.VISIBLE
                    binding.layoutEstimation.visibility=View.GONE
                    binding.layoutErreurAccommodation.visibility=View.GONE
                }
                is ImmoResult.Estimated -> {
                    binding.estimation.setText(getString(R.string.estime_result_text,value.price.toInt()))
                    binding.layoutEstimation.visibility=View.VISIBLE
                    binding.layoutErreur.visibility=View.GONE
                    binding.layoutErreurAccommodation.visibility=View.GONE
                }

            }
        }
    }
}