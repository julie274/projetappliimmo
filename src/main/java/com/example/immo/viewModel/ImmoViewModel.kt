package com.example.immo.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.example.immo.data.ImmoResult
import com.example.immo.model.ImmoBackendImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

private const val STATE_KEY_RESULT = "state_key_result"

@HiltViewModel
class ImmoViewModel @Inject constructor(
    state: SavedStateHandle,
    private val immoBackend: ImmoBackendImpl
    ): ViewModel() {

        /*
         * La MutableLiveData prend pour valeur la valeur de retour de la fonction estime du backend
         * La valeur est stockée dans une LiveData pour être observée dans la view sans être modifiée
         */

        private val _resultEstimation : MutableLiveData<ImmoResult> = state.getLiveData(STATE_KEY_RESULT)
        val resultEstimation: LiveData<ImmoResult> = _resultEstimation

        fun estime(s:String,p:Float,b:Float,l:Float){
            try {
                _resultEstimation.value = ImmoResult.Estimated(immoBackend.estime(s,p,b,l))
            } catch (e: Exception){

                /*
                 * Dans le cas ou les arguments ne sont pas correctement transmis des exceptions sont lancées
                 * NumberFormatException : si une valeur numérique est négative
                 * IllegalArgumentException : si le type de logement n'est pas correctement saisi
                 */

                when(e) {
                    is NumberFormatException -> _resultEstimation.value =
                        ImmoResult.FailedNumber(e.message!!)
                    is IllegalArgumentException -> _resultEstimation.value =
                        ImmoResult.FailedAccommodation(e.message!!)
                    else -> throw e
                }
            }
        }
}