package com.example.immo.model

import org.junit.Assert.*
import org.junit.Test

class ImmoBackendImplTest {

    /*
     * Tests unitaires du Backend
     */

    @Test
    fun `estime() return the price of a 2-room apartment with 100m2 of area and 3 lots`(){
        val immoBackend = ImmoBackendImpl()
        val result = immoBackend.estime("appartement",2f,100f,3f)
        assertEquals(result,424499.53f)
    }

    @Test
    fun `estime() returns the price of a 7-room apartment with 1000m2 of area and 5 lots`(){
        val immoBackend = ImmoBackendImpl()
        val result = immoBackend.estime("maison",7f,1000f,5f)
        assertEquals(result,1285575.4f)
    }
}