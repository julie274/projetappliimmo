Projet d’équipe 2022-2023

Développeurs : Lina Ferial MEGHOUCHE, Julie SOETEWEY

Ce projet a pour objectif de réaliser une application Android permettant d’estimer le prix d’une maison ou d’un appartement dans la commune de Saint-Etienne du Rouvray à partir de caractéristiques fournis par l’utilisateur à savoir :
- le type de logement
- le nombre de pièce
- la surface du bâtiment
- le nombre de lot

L’application a été programmé en Kotlin et le modèle a été codé en Python. Les logiciels utilisés lors de ce projet sont Android Studio et Jupyter-Notebook

Pour obtenir cette estimation, le modèle d’apprentissage utilisé a été la régression linéaire. Cette méthode a permis d’obtenir des coefficients à appliquer aux valeurs saisies par l’utilisateur pour estimer le bien.

Pour que l’application fonctionne il faut remplir tous les champs visibles sur l’écran d’accueil. Le premier doit être rempli avec le mot « maison » ou « appartement », et les suivants avec une valeur numérique positive ou nulle.

L’application pourrait être améliorée à l’avenir. Il est, par exemple, possible d’améliorer la précision de l’estimation notamment avec une méthode de classification plus élaborée. Il est également possible de rendre l’interface de l’application plus intuitive, par exemple, en remplaçant le champ textuel du type de logement par une liste déroulante.
